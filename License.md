# DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE Version 2.1, May 2019

*Copyright (C) 2019 John T Hill IV (jhill515@gmail.com)*

Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the copyright holder name is changed.

# DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
## TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. The terms "copying", "distributing", and "modifying" all have the same legal definition as recognized by U.S. Copyright Law at the time of this license version.

1. You just **DO WHAT THE FUCK YOU WANT TO**, just take my (the copyright holder's) name off of it.


## LIABILITY TERMS

0. By copying, distributing, and modifying, you accept all responsibilities of liability and absolve the above copyright holder of those liabilities.
