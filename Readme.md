<!-- @copyright Copyright (c)2019 John T Hill IV; All rights reserved.                                               -->
<!--                                                                                                                 -->
<!-- @project Trial and Error                                                                                        -->
<!-- @file    Readme.md                                                                                              -->
<!-- @brief   Project readme file.                                                                                   -->
<!--                                                                                                                 -->
<!-- @attention Do What the Fuck You Want to Public License (WTFPL)                                                  -->
<!-- @attention See License.md or request from John T Hill IV for license details.                                   -->



Introduction
========================================================================================================================
This just contains a random assortment of trial-and-error code implementations. They usually spawn from weird "what if" questions that either the author posed, or was posed to him. There is very little documentation by design, but naming is straightforward describing intent.

**Current Version:** [Enter At Your Own Risk](todo.org) (1.0.1) : May 2019


Contents
------------------------------------------------------------------------------------------------------------------------
- ConstMap : Just an attempt to play around with a simple constant map in C++ to answer a Stack Overflow question



Build & Installation Instructions
========================================================================================================================
Most of the directories within nor the source code has a build system. This is because some parts of the code are designed to never be able to compile; this is also because we're not sure if there is a specific tickle to a compilation command that changes behaviors. So, build this piecemill with your favorite compiler; note that this makes no advertisement that you're specific build setup will "work".


Dependencies
------------------------------------------------------------------------------------------------------------------------
Read the code and figure it out.


Testing Support
------------------------------------------------------------------------------------------------------------------------
**None**
